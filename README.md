### Created with Vite

---

# FE Clock challenge

Hello and thanks for participating in this coding challenge. This coding exercise will help us get a better feeling about your coding process.
If you have any questions, we're happy to help you. Just drop us an email.
## Instructions

An essential part of the challenge is also coming up with your own solutions and making reasonable assumptions about the implementation of the acceptance criteria.

Task points are build up on each other but you can think of the first 3 as must have and the rest of them as bonus challenges.

Please use vue or vanilla js.
## Task
1. Create a clock with visible hours, minutes and seconds.
2. The clock should update at least every second. For simplicity, it starts as soon as the page loads.
3. 100% responsive design but max width is 1000px. Please do not use any CSS frameworks.
4. It should update a store (vuex) state to have a text formatted value of the current quarter or hour half.
5. The current quarter and hour half should be visible on the screen and updated automatically once the store is updated. The text for these should appear as “1st quarter”, “2nd quarter” etc.
6. Make it an analog clock.
7. Animate seconds hand movements with css.
8. Add buttons: “+5 minutes” and “-10 minutes” and functionality for these.
9. The clock should continue where it was set with the buttons in 8. even after a page refresh.
